import java.util.Scanner;

public class Login {
    public boolean login(Account account){
        Scanner scan = new Scanner(System.in);
        System.out.print("Istifadeci adini daxil edin: ");
        String userName = scan.nextLine();
        System.out.print("Sifreni daxil edin: ");
        String password = scan.nextLine();
        if (userName.equals(account.getUserName()) && password.equals(account.getPassword())){
            return true;
        }
        else
            return false;
    }

}
