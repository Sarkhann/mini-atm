import java.util.Scanner;

public class Atm {
    public void run(Account account) {
        Login login = new Login();
        Scanner scan = new Scanner(System.in);
        System.out.println("Bankimiza xosh gelmisiz!");
        System.out.println("Hesab girisi");
        int count = 3;
        while (true) {
            if (login.login(account)) {
                System.out.println("Hesaba daxil oldunuz!");
                break;
            } else {

                System.out.println("Ad ve ya parol yanlisdir!");
                count--;
            }
            if (count == 0) {
                System.out.println("Giris melumatarini 3 defe sehv daxil etdiyiniz ucun kartiniz bloklanmisdir.\n"+
                                    "Blokdan cixarmaq ucun bank filiallarindan birine yaxinlasin");
                return;
            }
        }
        String operations = ("1. Balansi gostermek \n" +
                "2. Balansi artirmaq \n" +
                "3. Balansdan pul cixartmaq\n" +
                "0. Prosesi sonlandirmaq ");
        while (true) {
            System.out.println("**************************");
            System.out.println(operations);
            System.out.print("Emeliyyat nomresini daxil edin: ");
            String operation = scan.nextLine();
            if (operation.equals("0"))
                break;

            else if (operation.equals("1")){
                System.out.println( account.getBalance());
            }

            else if (operation.equals("2")) {
                System.out.println("Aritmaq istediyiniz meblegi daxil edin: ");
                int money = scan.nextInt();
                scan.nextLine();
                account.increaseMoney(money);

            }

            else if (operation.equals("3")) {
                System.out.print("Balansdan cixarmaq istediyiniz mebledi daxil edin:");
                int money = scan.nextInt();
                scan.nextLine();
                account.recudeMoney(money);

            }

            else {
                System.out.println("Bu nomreli emeliyyat tapilmadi. Nomreni duzgun daxil etdiyinizden emin olun!");

            }
        }
    }
}
