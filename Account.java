public class Account {

    private String userName;
    private String password;
    private double balance;

    public Account(String userName, String password, double balance) {
        this.userName = userName;
        this.password = password;
        this.balance = balance;
    }

    public void increaseMoney(double money) {
        balance += money;
        System.out.println("Yekun balans: " + balance);
    }

    public void recudeMoney(double money) {
        if (money > 2000) {
            System.out.println("Bir defeye kartdan " + money + " pul cixara bilmezsiz. Birdefeye max 2000 mumkundur.");
        }
        else if (balance - money < 0) {
            System.out.println("Balansinizda yeterli mebleg yoxdur. Sinin balansiniz: " + balance);;
        }
        else {
            balance-=money;
            System.out.println("Yekun balans: " + balance);;
        }

    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return this.balance;
    }

}
